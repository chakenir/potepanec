class Potepan::ProductsController < ApplicationController
  MAX_RELATED_NUMBER = 4

  def show
    @product = Spree::Product.includes(master: [:images, :default_price]).find(params[:id])
    @product_option_types = @product.option_types
    @related_products = Spree::Product.related_products(@product).limit(MAX_RELATED_NUMBER)
  end
end
