require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given!(:taxon) { taxonomy.root }
  given!(:taxonomy) { create(:taxonomy) }
  given!(:product) { create(:product, taxons: [taxon]) }
  given!(:bag) { taxonomy.root.children.create(name: "bag") }
  given!(:mug) { taxonomy.root.children.create(name: "mug") }
  given!(:shirt) { taxonomy.root.children.create(name: "shirt") }

  background do
    visit potepan_category_path taxon.id
  end

  scenario "サイドバーに商品カテゴリが表示される" do
    within '.side-nav' do
      expect(page).to have_content taxon.name
      expect(page).to have_content taxonomy.name
      expect(page).to have_content bag.name
      expect(page).to have_content mug.name
      expect(page).to have_content shirt.name
    end
  end

  scenario "商品名をクリックすると、商品ページに移動する" do
    expect(page).to have_content product.name
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario "サイドバーの商品カテゴリをクリックすると、カテゴリページに移動する" do
    within '.side-nav' do
      click_link bag.name
      expect(current_path).to eq potepan_category_path(bag.id)
      expect(page).to have_title "#{bag.name} | BIGBAG"
      click_link mug.name
      expect(current_path).to eq potepan_category_path(mug.id)
      expect(page).to have_title "#{mug.name} | BIGBAG"
      click_link shirt.name
      expect(current_path).to eq potepan_category_path(shirt.id)
      expect(page).to have_title "#{shirt.name} | BIGBAG"
    end
  end
end
