require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:taxon) { create :taxon }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product) }

  background do
    visit potepan_product_path product.id
  end

  scenario "商品情報・関連商品がタイトルタグとページ内に動的に表示される（されない）" do
    expect(page).to have_title product.name
    expect(page).to have_content product.name
    expect(page).to have_content product.price
    expect(page).to have_content product.description
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.display_price
    expect(page).not_to have_content other_product.name
  end

  scenario "オプションがページ内に動的に表示され、一致する" do
    product.option_types.each do |option_type|
      option_type.option_values.each do |option_value|
        expect(page).to have_content option_value.presentation
      end
    end
  end

  scenario "「一覧ページへ戻る」をクリックすると、商品一覧ページに戻る" do
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(related_product.taxons.first.id)
  end

  scenario "関連商品の商品名をクリックすると、商品詳細ページに移動する" do
    click_link related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
