require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show controller @taxon' do
    let!(:taxon) { taxonomy.root }
    let!(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product) }
    let!(:product_with_specified_taxon) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it '正常なアクセスができる' do
      expect(response).to have_http_status(:ok)
    end

    it 'showテンプレートを表示する' do
      expect(response).to render_template(:show)
    end

    it 'taxonを取得する' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'taxonomiesを取得する' do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it 'productsを取得する' do
      expect(assigns(:products)).to match_array product_with_specified_taxon
    end
  end
end
