require 'rails_helper'

class Product < ActiveRecord::Base
  has_many :option_types
end

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let!(:taxon) { create(:taxon) }
    let!(:option_type) { create(:option_type) }
    let!(:product) { create(:product, option_types: [option_type], taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 6, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it '正常なアクセスができる' do
      expect(response).to have_http_status(:ok)
    end

    it 'showテンプレートを表示する' do
      expect(response).to render_template(:show)
    end

    it 'productをテストする' do
      expect(assigns(:product)).to eq product
    end

    it 'product_option_typesをテストする' do
      product_option_types = product.option_types
      expect(assigns(:product_option_types)).to eq product_option_types
    end

    it 'related_productsをテストする' do
      expect(assigns(:related_products).size).to eq 4
    end
  end
end
