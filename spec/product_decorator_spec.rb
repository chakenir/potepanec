require 'rails_helper'

RSpec.describe Spree::Product, type: :decorator do
  describe 'related_product' do
    let(:taxon_ruby) { create(:taxon, name: "ruby") }
    let(:taxon_apache) { create(:taxon, name: "apache") }
    let(:taxon_rails) { create(:taxon, name: "rails") }
    let(:product_with_ruby) { create(:product, taxons: [taxon_ruby]) }
    let(:product_with_apache) { create(:product, taxons: [taxon_apache]) }
    let(:product_with_ruby_apache) { create(:product, taxons: [taxon_ruby, taxon_apache]) }
    let(:product_with_ruby_rails) { create(:product, taxons: [taxon_ruby, taxon_rails]) }

    it 'カテゴリと商品が一致する' do
      expect(Spree::Product.related_products(product_with_ruby)).to match_array([product_with_ruby_apache, product_with_ruby_rails])
    end

    it 'カテゴリと商品が一致しない' do
      expect(Spree::Product.related_products(product_with_ruby)).not_to include product_with_apache
    end

    it 'カテゴリの中にその商品が含まれない' do
      expect(Spree::Product.related_products(product_with_ruby)).not_to include product_with_ruby
    end
  end
end
